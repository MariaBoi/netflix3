const User = require("../models/User");
const jwt = require("jsonwebtoken");
SECRET_FOR_ENCR = process.env.SECRET_FOR_ENCR;
const getUsers = async (req, res) => {
  let myusers;

  try {
    myusers = await User.find();
  } catch (e) {
    console.log(e);
    return res.status(500).json({ message: "server error " });
  }

  return res.status(200).json({ message: "success", users: myusers });
};

const getUserFromToken = async (req, res) => {
  try {
    const payload = jwt.verify(req.cookies.token, SECRET_FOR_ENCR);
    const myuser = await User.findById(payload._id);

    return res.status(200).json({ message: "success", user: myuser });
  } catch (e) {
    console.log(e);
    return res.status(401).json({ message: "укк " });
  }
};

module.exports = { getUsers, getUserFromToken };

const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const CustomError = require("../utils.js/CustomError");

generateToken = (userdb) => {
  SECRET_FOR_ENCR = process.env.SECRET_FOR_ENCR;

  return jwt.sign(
    {
      username: userdb.username,
      _id: userdb._id,
      favoriteFilms: userdb.favoriteFilms,
      followings: userdb.followings,
    },
    SECRET_FOR_ENCR,
    { expiresIn: 24000 }
  );
};

class Auth {
  async registerUser(req, res) {
    if (!req.body.username || !req.body.password) {
      return res.status(400).json({ message: "pass or username is not here" });
    }

    try {
      const userExists = await User.findOne({
        username: req.body.username,
      });

      if (userExists) {
        throw new CustomError(400, "це ім'я користувача вже зайнято");
      }

      const hashedPass = await bcrypt.hash(req.body.password, 10);

      const userToSend = new User({
        username: req.body.username,
        password: hashedPass,
      });
      const userdb = await userToSend.save();

      res.status(200).json({
        message: "success",
        user: userToSend,
        jwt_token: generateToken(userToSend),
      });
    } catch (error) {
      console.log("err1111", error);
      res.status(400).json({ message: error.message });
    }
  }

  async login(req, res) {
    let userdb;
    try {
      userdb = await User.findOne({ username: req.body.username });

      if (!userdb) {
        return res.status(400).json({ message: "no such user" });
      }
      const isPassvalid = await bcrypt.compare(
        req.body.password,
        userdb.password
      );

      if (!isPassvalid) {
        return res.status(400).json({ message: "password is wrong" });
      }
    } catch (e) {
      return res.status(500).json({ message: "server error", e });
    }

    res.status(200).json({
      message: "success",
      jwt_token: generateToken(userdb),
      user: userdb,
    });
  }
}

module.exports = new Auth();

const User = require("../models/User");

const toggleFriend = async (req, res) => {
  try {
    const curuser = await User.findById(req.params.id);
    if (!curuser.followings.includes(req.body.userId)) {
      await curuser.updateOne({ $push: { followings: req.body.userId } });
      const updUser=  await  User.findById(req.params.id)
      return res
        .status(200)
        .json({ mes: "added to fiendas", followings: updUser.followings });
    } else {
      await curuser.updateOne({ $pull: { followings: req.body.userId } });
      const updUser=  await  User.findById(req.params.id)

      return res
        .status(200)
        .json({ mes: "removed from friens", followings: updUser.followings });
    }
  } catch (e) {
    console.log(e);
    res.status(500).json(e);
  }
};

module.exports = { toggleFriend };

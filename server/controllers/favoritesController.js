const User = require("../models/User");

const toggleFavorite = async (req, res) => {
  try {
    const curuser = await User.findById(req.params.id);
    if (!curuser.favoriteFilms.includes(req.body.name)) {
    await curuser.updateOne({ $push: { favoriteFilms: req.body.name } });
    const updUser=  await  User.findById(req.params.id)
      return res
        .status(200)
        .json({ mes: "added to fiendas", favoriteFilms: updUser.favoriteFilms });
    } else {

      await curuser.updateOne({ $pull: { favoriteFilms: req.body.name } });
    const updUser=  await  User.findById(req.params.id)

      return res
        .status(200)
        .json({ mes: "removed from friens", favoriteFilms:updUser.favoriteFilms});
    }
  } catch (e) {
    console.log(e);
    res.status(500).json(e);
  }
};

module.exports = { toggleFavorite };

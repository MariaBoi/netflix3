require("dotenv").config();
var cors = require("cors");
const express = require("express");
const fs = require("fs");
const path = require("path");
const mongoose = require("mongoose");
const morgan = require("morgan");
const authRouter = require("./routes/auth.route");
const usersRouter = require("./routes/users.route");
const { toggleFriend } = require("./controllers/friendsController");
const cookieParser = require("cookie-parser");
const { toggleFavorite } = require("./controllers/favoritesController");
const PORT = process.env.PORT;
const app = express();

mongoose.connect(
  process.env.DB_CONNECTION,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => {
    console.log("connected to mongo ");
  }
);

const pathToFile = path.join(__dirname, "logger.txt");
if (!fs.existsSync(pathToFile)) {
  fs.appendFileSync("logger.txt", "", "utf-8");
}
const myStream = fs.createWriteStream(pathToFile, { flags: "a" });
app.use(morgan("combined", { stream: myStream }));
app.use(
  cors({
    credentials: true,
    origin: "http://localhost:3000",
  })
);
app.use(express.json());
app.use(cookieParser());
app.use("/netflix/api/auth", authRouter);
app.use("/netflix/api/user", usersRouter);
app.put("/netflix/api/:id/togglefriend", toggleFriend);
app.put("/netflix/api/:id/togglefavorite", toggleFavorite);

app.use((err, req, res, next) => {
  console.error(err.stack);
  if (err.status && err.message) {
    res.status(err.status).json({
      message: err.message,
    });
    return;
  }
  res.status(500).json({
    message: "Internal server error",
  });
});

app.listen(PORT || 8080, () => console.log("listening" + PORT));

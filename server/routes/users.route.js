const { getUsers, getUserFromToken } = require("../controllers/usersControllers");

const usersRouter = require("express").Router();

usersRouter.get("/all", getUsers);
usersRouter.get("/", getUserFromToken);

module.exports = usersRouter;

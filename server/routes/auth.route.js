const auth = require("../controllers/authControllerClass");
const authRouter = require("express").Router();

authRouter.post("/register", auth.registerUser);
authRouter.post("/login", auth.login);

module.exports = authRouter;

import axios from "axios";
import { useCallback, useContext, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../context/AuthContextProvider";
import FavContext from "../context/FavContextProvider";

import { FavoriteButton } from "./FvoriteButton";
import classes from "./ListItem.module.css";

const ListItem = ({ image, name, rating, id, favspage }) => {
  const favCtxObj = useContext(FavContext);
  const [liked, setLiked] = useState(false);

  //console.log('dddddddddddfavvvvvvvvv',favCtxObj)
  const authContextObj = useContext(AuthContext);
  const current = authContextObj.user;

  const itemisfav = favCtxObj.favs.includes(name)

  const handleSaveFavs = async () => {
    try {
      const response = await axios.put(
        `http://localhost:8080/netflix/api/${current._id}/togglefavorite`,
        { name }
      );
      favCtxObj.setuserFavs([...response.data.favoriteFilms]);
    } catch (e) {
      console.log(e);
    }
  };
  //};

  const expr = favspage && !itemisfav;

  return expr ? (
    ""
  ) : (
    <div>
      <Link to={`/singleshow/${id}`} className={classes.listitem}>
        <div className={classes.listitem__info}>
          <h4 className={classes.info__name}>{name}</h4>
          <h4 className={classes.info__rating}>{rating}</h4>
        </div>
        <img src={image} alt={name} />
      </Link>
      {authContextObj.user && (
        <>
          <FavoriteButton togglefav={handleSaveFavs} itemisfav={itemisfav} />
          <button
            className={classes.liked}
            onClick={() => setLiked((prev) => !prev)}
          >
            <i className={liked ? "fas fa-heart" : "far fa-heart"}></i>
          </button>
        </>
      )}
    </div>
  );
};

export default ListItem;

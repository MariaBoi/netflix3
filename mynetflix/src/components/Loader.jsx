import classes from "./Loader.module.css";

const Loader = () => {
  return (
    <div className={classes.loader}>
      <img
        className={classes.loader__img}
        src="https://c.tenor.com/hQz0Kl373E8AAAAj/loading-waiting.gif"
        alt="Loading..."
      />
    </div>
  );
};

export default Loader;

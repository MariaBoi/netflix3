export const InputFilter = ({ arrOfChoices, label, handleChange, valuei }) => {
  return (
    <div className="">
      <label>фільтрувати за {label}</label>
      <select value={valuei} onChange={handleChange} className="btn-u">
        <option>all</option>
        {arrOfChoices.map((d, i) => (
          <option key={i} value={d.tag}>
            {d.tag}
          </option>
        ))}
      </select>
    </div>
  );
};
export default InputFilter;

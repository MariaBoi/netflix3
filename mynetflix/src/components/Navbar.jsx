import { NavLink } from "react-router-dom";
import { useCookies } from "react-cookie";
import {
  ABOUT_ROUTE,
  HOME_ROUTE,
  LOGIN_ROUTE,
  REGIST_ROUTE,
} from "../registr/consts";
import React, { useContext } from "react";

import classes from "./Navbar.module.css";
import { AuthContext } from "../context/AuthContextProvider";

const Navbar = () => {
  const [cookies, setCookie, removeCookie] = useCookies([""]);
  const { user, setUser } = useContext(AuthContext);
  const logout = () => {
    setUser(null);
    setCookie("token", "");
  };

  return (
    <div className={classes.navbar}>
      <div className={classes.container}>
        <nav className={classes.navbar__nav}>
          <h3 className={classes.nav__brand}>
            <NavLink to="/login">
              <i className="fas fa-tv"></i> Стрімінг сервіс
            </NavLink>
          </h3>
          <ul className={classes.nav__links}>
            <li className={classes.links__link}>
              <NavLink to={HOME_ROUTE}>всі фільми</NavLink>
            </li>
            <li className={classes.links__link}>
              <NavLink to={ABOUT_ROUTE}>користувачі</NavLink>
            </li>

            <li className={classes.links__link}>
              <NavLink to="/cab">особистий кабінет</NavLink>
            </li>

            {user ? (
              <>
                <h3 className={classes.nicknav}>{user.username}</h3>

                <button onClick={logout} className="double-bor-btn">
                  Вийти з сайту
                </button>
              </>
            ) : (
              <>
                <NavLink to={LOGIN_ROUTE}>
                  <button className="double-bor-btn">зайти </button>
                </NavLink>
                <NavLink to={REGIST_ROUTE}>
                  <button className="double-bor-btn">зареєструватися</button>
                </NavLink>
              </>
            )}
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;

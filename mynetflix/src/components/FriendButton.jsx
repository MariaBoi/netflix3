import axios from "axios";
import React, { useCallback, useContext } from "react";
import { AuthContext } from "../context/AuthContextProvider";
import classes from "./FriendButton.module.css";
export const FriendButton = ({
  id,
  username,
  friendsstate,
  setfriendsstate,
  current,

  favoriteFilms,
}) => {
  const authContextObj = useContext(AuthContext);

  const isfriendHandler = friendsstate?.some((el) => el === id);

  const togglefriend = async () => {
    if (current._id) {
      const res = await axios.put(
        `http://localhost:8080/netflix/api/${current._id}/togglefriend`,
        { userId: id }
      );
      setfriendsstate(res.data.followings);
    }
  };

  return (
    <>
      <li className={classes.user_item}>
        <p className={classes.user_email}> {username}</p>
        {authContextObj.user && (
          <>
            <button onClick={togglefriend} className={classes.add_button}>
              <i className="fas fa-user-friends"></i>
              {isfriendHandler
                ? " видалити з користувача друзів"
                : " додати до друзів"}
            </button>
            {isfriendHandler ? (
              <>
                <p> цьому користувачу подобаєтся:</p>
                {favoriteFilms?.map((el2, i) => (
                  <ul>
                    <li className={classes.fav_item} key={i}>
                      <span className={classes.star}>
                        <i className="fas fa-star"></i>
                      </span>
                      {el2}
                    </li>
                  </ul>
                ))}
              </>
            ) : (
              <p>додайте даного користувача в друзі, щоб бачити його вибране</p>
            )}
          </>
        )}
      </li>
    </>
  );
};

import { useContext, useEffect, useState } from "react";
import { Listshows } from "../components/Listshows.jsx";
import Axios from "axios";
import { LoaderContext } from "../context/LoaderContextProvider.jsx";
import { ModalFil } from "../components/ModalFil.jsx";

const Homepage = () => {
  const { setIsLoading } = useContext(LoaderContext);
  const [allfilms, setallfilms] = useState([]);
  useEffect(() => {
    setIsLoading(true);
    Axios.get(`https://api.tvmaze.com/shows`).then((respon) => {
      setallfilms(respon.data);

      setIsLoading(false);
    });
  }, []);

  return (
    <div className="homepage">
      <ModalFil />
      <Listshows shows={allfilms} />
    </div>
  );
};

export default Homepage;

import Axios from "axios";
import { useEffect, useContext, useState, useCallback } from "react";
import { LoaderContext } from "../context/LoaderContextProvider.jsx";
import classes from "./Singlepage.module.css";

const Singlepage = ({ match }) => {
  console.log("SINGLE RENDER");
  const [singleShow, setsingle] = useState([]);
  const { setIsLoading } = useContext(LoaderContext);
  useEffect(() => {
    setIsLoading(true);
    Axios.get(`https://api.tvmaze.com/shows/${match.params.id}`).then(
      (respon) => {
        setsingle(respon.data);
        setIsLoading(false);
      }
    );
  }, []);

  const removeTags = useCallback((text) => {
    if (text === null || text === "") {
      return false;
    }
    return text.toString().replace(/(<([^>]+)>)/gi, "");
  }, []);

  return (
    <>
      <div className={classes.singleshow}>
        <img
          src={
            singleShow.image
              ? singleShow.image.medium
              : "https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg"
          }
          alt={singleShow.name}
        />
        <div className={classes.singleshow__info}>
          <h1>{singleShow.name}</h1>
          {singleShow.genres &&
            singleShow.genres.map((genre) => (
              <span key={genre} className={classes.singleshow__genre}>
                {genre}
              </span>
            ))}
          <p>
            <strong>Status:</strong> {singleShow.status}
          </p>
          <p>
            <strong>Rating:</strong>{" "}
            {singleShow.rating ? singleShow.rating.average : "No rating"}
          </p>
          <p>
            <strong>Language:</strong>{" "}
            {singleShow.language ? singleShow.language : "No lang"}
          </p>
          <p>
            <strong>Offical Site:</strong>{" "}
            {singleShow.officalSite ? (
              <a href={singleShow.officalSite} target="_blank" rel="noreferrer">
                {singleShow.officalSite}
              </a>
            ) : (
              "No offical site"
            )}
          </p>
          <p>{singleShow.summary && removeTags(singleShow.summary)}</p>
        </div>
      </div>
    </>
  );
};

export default Singlepage;

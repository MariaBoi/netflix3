import { useContext, useEffect, useState } from "react";
import { FriendButton } from "../components/FriendButton.jsx";
import { AuthContext } from "../context/AuthContextProvider.jsx";
import { LoaderContext } from "../context/LoaderContextProvider.jsx";
import Axios from "axios";
import classes from "./Userspage.module.css";

const Userspage = () => {

  const authContextObj = useContext(AuthContext);
  const [friendsstate, setfriendsstate] = useState([]);
  const [allUsers, setAllUsers] = useState([]);
  const { setIsLoading } = useContext(LoaderContext);
  const current = authContextObj.user;
  useEffect(async() => {

    setfriendsstate(current?.followings ?? []);
    setIsLoading(true);
    const respon=await  Axios.get("http://localhost:8080/netflix/api/user/all")
      const data = respon.data;
    setAllUsers(data.users);
      setIsLoading(false);
  }, [authContextObj.user]);

  return (
    <div className={classes.container}>
      <h3 className={classes.title}>зареєстровані користувачі:</h3>

      <ul className={classes.films_list}>
        {allUsers
          .filter((item) => item._id !== current?._id)
          .map((el, ind) => (
            <FriendButton
              current={current}
              key={ind}
              id={el._id}
              username={el.username}
              friendsstate={friendsstate}
              setfriendsstate={setfriendsstate}
              favoriteFilms={el.favoriteFilms}
            />
          ))}
      </ul>
    </div>
  );
};

export default Userspage;

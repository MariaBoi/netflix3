import { BrowserRouter as Router } from "react-router-dom";

import Navbar from "./components/Navbar";

import AppRouter from "./registr/AppRouter";

const App = () => {
  return (
    <Router>
      <Navbar />
      <div className="container">
        <AppRouter />
      </div>
    </Router>
  );
};

export default App;

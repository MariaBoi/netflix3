import {
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import Loader from "../components/Loader";

export const LoaderContext = createContext();

const LoaderContextProvider = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const value = useMemo(() => ({ isLoading, setIsLoading }), [isLoading]);
  return (
    <LoaderContext.Provider value={value}>
      {isLoading && <Loader />} {props.children}
    </LoaderContext.Provider>
  );
};

export default LoaderContextProvider;

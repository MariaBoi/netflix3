import { createContext, useMemo, useState } from "react";

const FavContext = createContext({
  favs: [],
  isfav: (p) => {},
});

export const FavContextProvider = (props) => {
  const [userFavs, setuserFavs] = useState([]);

  const context = useMemo(
    () => ({
      favs: userFavs,
      setuserFavs,
    }),
    [userFavs]
  );

  return (
    <FavContext.Provider value={context}>{props.children}</FavContext.Provider>
  );
};

export default FavContext;

import { useState, createContext, useMemo, useContext, useEffect } from "react";
import { LoaderContext } from "./LoaderContextProvider";
import axios from "axios";
import FavContext from "./FavContextProvider";
export const AuthContext = createContext();

export const AuthContextProvider = (props) => {
  const [user, setUser] = useState(null);
  const { setIsLoading } = useContext(LoaderContext);
  const { setuserFavs } = useContext(FavContext);

  useEffect(() => {
    setIsLoading(true);
    axios
      .get("http://localhost:8080/netflix/api/user", { withCredentials: true })
      .then((response) => {
        const user = response.data.user;
        setuserFavs(user.favoriteFilms);
        setUser(user);
        setIsLoading(false);
      });
  }, []);
  const value = useMemo(() => ({ user, setUser }), [user, setUser]);
  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  );
};

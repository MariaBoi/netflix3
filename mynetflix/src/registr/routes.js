import {
  ABOUT_ROUTE,
  CAB_ROUTE,
  HOME_ROUTE,
  LOGIN_ROUTE,
  REGIST_ROUTE,
  SINGLEP_ROUTE,
} from "./consts";
import Login from "./Login.jsx";
import Cabinet from "./Cabinet.jsx";
import Singlepage from "../pages/Singlepage.jsx";
import UsersPage from "../pages/Userspage.jsx";
import HomePage from "../pages/Homepage.jsx";
import { RegistPage } from "./RegistPage.jsx";

export const allRoutes = [
  {
    path: HOME_ROUTE,
    Component: HomePage,
  },

  {
    path: ABOUT_ROUTE,
    Component: UsersPage,
  },
  { path: SINGLEP_ROUTE, Component: Singlepage, exactvar: true },
];

export const privateRoutes = [
  {
    path: CAB_ROUTE,
    Component: Cabinet,
  },
];

export const publicRoutes = [
  {
    path: LOGIN_ROUTE,
    Component: Login,
  },
  { path: REGIST_ROUTE, Component: RegistPage },
];

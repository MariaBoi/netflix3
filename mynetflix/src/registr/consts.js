export const LOGIN_ROUTE = "/login";
export const CAB_ROUTE = "/cab";
export const HOME_ROUTE = "/";
export const ABOUT_ROUTE = "/users";
export const SINGLEP_ROUTE = "/singleshow/:id";
export const REGIST_ROUTE = "/register";

//=======================

export const FILER_GENRE = "FILER_GENRE";
export const FILER_STATUS = "FILER_STATUS";
export const FILER_LANG = "FILER_LANG";

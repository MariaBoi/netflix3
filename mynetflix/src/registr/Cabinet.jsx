import Axios from "axios";
import React, { useContext, useState, useEffect } from "react";
import { Listshows } from "../components/Listshows.jsx";
import { LoaderContext } from "../context/LoaderContextProvider.jsx";

const Cabinet = () => {
  const { setIsLoading } = useContext(LoaderContext);
  const [filmshp, setfilmshp] = useState([]);

  useEffect(() => {
    setIsLoading(true);

    Axios.get(`https://api.tvmaze.com/shows`).then((respon) => {
      setfilmshp(respon.data);
      setIsLoading(false);
    });
  }, []);

  return (
    <div style={{ "margin-top": "1.6rem" }}>
      вітаємо! це ваш особистий кабінет тепер ви можете бачити список фільмів,
      що сподобались!
      <Listshows shows={filmshp} favspage={true} />
    </div>
  );
};

export default Cabinet;

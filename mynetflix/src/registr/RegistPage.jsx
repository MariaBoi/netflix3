import { useCallback, useContext, useState } from "react";
import axios from "axios";
import { AuthContext } from "../context/AuthContextProvider";
import FavContext from "../context/FavContextProvider";
import { useCookies } from "react-cookie";
export const RegistPage = () => {
  const [cookies, setCookie, removeCookie] = useCookies([""]);
  const [registered, setregistered] = useState(false);
  const { setUser, user } = useContext(AuthContext);
  const { setuserFavs } = useContext(FavContext);
  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();

    const { username, password } = e.target.elements;

    if (username.value.length < 4 || password.value.length < 4) {
      alert("пароль і username повинні бути не менше 4 символів");
      return;
    }
    let res;
    try {
      res = await axios.post(
        "http://localhost:8080/netflix/api/auth/register",
        {
          username: username.value,
          password: password.value,
        }
      );

      setUser(res.data.user);
      setCookie("token", res.data.jwt_token);
      setregistered(true);
      setuserFavs([]);
    } catch (e) {
      alert(e.response.data.message);
    }
  }, []);

  return (
    <>
      {registered ? (
        <h1 className="cab-h"> ви зареєструвалися {user}</h1>
      ) : (
        <form onSubmit={handleSubmit}>
          <h1 className="cab-h">зареєструватися</h1>
          <input name="username" placeholder="username" type="text" />
          <input name="password" placeholder="password" type="password" />
          <button type="submit" className="double-bor-btn butt-lightback">
            зареєструватися
          </button>
        </form>
      )}
    </>
  );
};

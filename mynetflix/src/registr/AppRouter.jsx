import React, { useContext } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { allRoutes, privateRoutes, publicRoutes } from "./routes";
import { CAB_ROUTE, LOGIN_ROUTE } from "./consts";
import { AuthContext } from "../context/AuthContextProvider";


const AppRouter = () => {
  const authContextObj = useContext(AuthContext);

  return authContextObj.user ? (
    <Switch>
      {allRoutes.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} exact={true} />
      ))}
      {privateRoutes.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} exact={true} />
      ))}
      <Redirect to={CAB_ROUTE} />
    </Switch>
  ) : (
    <Switch>
      {allRoutes.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} exact={true} />
      ))}

      {publicRoutes.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} exact={true} />
      ))}
      <Redirect to={LOGIN_ROUTE} />
    </Switch>
  );
};

export default AppRouter;

import { useCallback, useContext } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import { AuthContext } from "../context/AuthContextProvider";
import FavContext from "../context/FavContextProvider";

const Login = () => {
  const [cookies, setCookie, removeCookie] = useCookies([""]);
  const { setuserFavs } = useContext(FavContext);
  const { setUser } = useContext(AuthContext);
  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();

    const { username, password } = e.target.elements;

    try {
      const res = await axios.post(
        "http://localhost:8080/netflix/api/auth/login",
        {
          username: username.value,
          password: password.value,
        }
      );

      setUser(res.data.user);
      setCookie("token", res.data.jwt_token);
      setuserFavs(res.data.user.favoriteFilms);
    } catch (e) {
      alert("невірний логін або пароль");
    }
  }, []);

  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1 className="cab-h">Зайти зі свого аккаунту</h1>

        <input name="username" placeholder="username" type="text" />
        <input name="password" placeholder="password" type="password" />
        <button type="submit" className="double-bor-btn butt-lightback">
          зайти з аккаунту
        </button>
      </form>
    </>
  );
};

export default Login;

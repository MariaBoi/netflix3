import ReactDOM from "react-dom";
import "./index.css";
import App from "./App.jsx";
import reportWebVitals from "./reportWebVitals";

import LoaderContextProvider from "./context/LoaderContextProvider.jsx";

import { FilterFilmsContextProvider } from "./context/FilterFilmsContextProvider.jsx";
import { FavContextProvider } from "./context/FavContextProvider";
import { AuthContextProvider } from "./context/AuthContextProvider";

ReactDOM.render(
  <FilterFilmsContextProvider>
    <LoaderContextProvider>
    <FavContextProvider>

      <AuthContextProvider>
      
            <App />
       
      </AuthContextProvider>
      </FavContextProvider>

    </LoaderContextProvider>
  </FilterFilmsContextProvider>,
  document.getElementById("root")
);

reportWebVitals();
